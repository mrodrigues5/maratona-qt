#include "mainwindow.h"

#include <QApplication>
#include <QtQuick3D/qquick3d.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSurfaceFormat::setDefaultFormat(QQuick3D::idealSurfaceFormat());
    MainWindow w;
    w.show();
    return a.exec();
}
