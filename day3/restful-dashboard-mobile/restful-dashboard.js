var appid=""

function updateCharts(jsonFile) {
    valueAxisX.clear()
    var days = valueAxisX.categories
    for (var i = 0; i < jsonFile.daily.length; ++i)
        days[i] = ((i === jsonFile.daily.length-1) ? " ":"") + Date.fromLocaleDateString((new Date(jsonFile.daily[i].dt*1000)).toLocaleDateString()).toLocaleString(Qt.locale("en_US"), "ddd")
    valueAxisXToday.clear()
    var hours = valueAxisXToday.categories
    for (i = 0; i < 24; ++i)
        hours[i] = Date.fromLocaleString((new Date(jsonFile.hourly[i].dt*1000)).toLocaleString()).toLocaleString(Qt.locale("en_US"), "hh")
    updateSeries(minSeries, valueAxisYTemp, "temp.min", jsonFile.daily, jsonFile.daily.length)
    updateSeries(maxSeries, valueAxisYTemp, "temp.max", jsonFile.daily, jsonFile.daily.length)
    updateSeries(rainfallSet, valueAxisYRain, "rain", jsonFile.daily, jsonFile.daily.length)
    updateSeries(lineSeries, valueAxisYLine, lineSeriesVariable.currentValue, jsonFile.hourly, 24)
    updateSeries(barSet, valueAxisYBar, barSeriesVariable.currentValue, jsonFile.hourly, 24)
}

function updateWeather(currentValue) {
    currentWeather = undefined
    if (appid !== "") {
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                currentWeather = JSON.parse(xhr.responseText)
                var xhr2 = new XMLHttpRequest()
                xhr2.onreadystatechange = function () {
                    if (xhr2.readyState === XMLHttpRequest.DONE && xhr2.status === 200) {
                        detailedWeather = JSON.parse(xhr2.responseText)
                        updateCharts(detailedWeather)
                    }
                }
                xhr2.open("GET", "https://api.openweathermap.org/data/2.5/onecall?lat=" + currentWeather.coord.lat + "&lon=" + currentWeather.coord.lon + "&units=metric&appid=" + appid)
                xhr2.send()
            }
        }
        xhr.open("GET", "https://api.openweathermap.org/data/2.5/weather?q=" + currentValue + ",br&units=metric&appid=" + appid)
        xhr.send()
    } else { // Use offline data
        currentWeather = OD.current[cityComboBox.currentIndex]
        detailedWeather = OD.sevenDays[cityComboBox.currentIndex]
        updateCharts(detailedWeather)
    }
}

function updateSeries(series, axis, seriesVariable, collection, collectionSize) {
    if (detailedWeather === undefined) return
    var max = 0; var min = Number.MAX_VALUE
    if (series instanceof LineSeries) series.clear()
    else series.remove(0, barSet.count)
    for (var i = 0; i < collectionSize; ++i) {
        var item = collection[i]
        seriesVariable.split('.').forEach((variable) => {
            item = item[variable.toLowerCase().replace(' ', '_')] ?? 0
        })
        if (series instanceof LineSeries) series.append(i, item)
        else series.append(item)
        if (item > max) max = item
        if (item < min) min = item
    }
    axis.max = max > 0 ? max*1.1:1
    axis.min = min > 0 ? min*0.9:0
}
