import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    property var cameras: QtMultimedia.availableCameras
    property int currentCamera: 0

    ColumnLayout {
        anchors { fill: parent; margins: spacing }
        VideoOutput {
            Layout.fillHeight: true; Layout.fillWidth: true
            source: Camera { id: camera }
            autoOrientation: true
        }
        RowLayout {
            width: parent.width
            Label {
                Layout.fillWidth: true
                text: camera.displayName
            }
            Button {
                text: "Change Camera"
                visible: cameras.length > 1
                onClicked: camera.deviceId = cameras[++currentCamera % cameras.length].deviceId // Fix me
            }
        }
    }
}
