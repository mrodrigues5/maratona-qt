import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15
import QtQuick.Shapes 1.15
import QtMultimedia 5.15

import Box2D 2.0

Image {
    id: rootItem
    width: 960; height: 540
    source: "images/bg" + Math.trunc(Math.random()*4) + ".png"

    property int runs: 0    
    property bool dirty: false
    property bool youWon: false
    property int level: 3

    MediaPlayer { id: bg; source: "audio/bg.mp3"; autoPlay: true; loops: MediaPlayer.Infinite }
    MediaPlayer { id: lost; source: "audio/lost.mp3" }
    MediaPlayer { id: win; source: "audio/win.mp3" }
    MediaPlayer { id: whoosh; source: "audio/whoosh.mp3" }
    MediaPlayer { id: collision; source: "audio/collision.mp3" }
    
    CheckBox {
        id: debugdraw
        Material.foreground: "white"
        text: "Debug draw"
    }

    World {
        running: banner.scale === 0
        onStepped: {
            if (!dirty || ogre.body.linearVelocity !== Qt.point(0, 0)) return
            for (var i = 0; i < blocks.children.length; ++i) {
                var block = blocks.children[i]
                if (block instanceof ImageBody && block.body.linearVelocity !== Qt.point(0, 0))
                    return
                if (block instanceof ImageBody && Math.trunc(block.y) < 444) {
                    ogre.x = 20; ogre.y = 411
                    sight.x = ogre.width + 20; sight.y = -20
                    dirty = false
                    lost.play()
                    runs++
                    banner.scale = 1
                    return
                }
            }
            youWon = true
            win.play()
            banner.scale = 1
        }
    }

    ImageBody {
        id: ogre
        x: 20; y: 411
        source: "images/ogre.png"
        height: parent.height/5
        body.fixedRotation: true
        onBeginContact: if (dirty) collision.play()
        Rectangle {
            id: sight
            width: 20; height: 20; radius: 10
            x: ogre.width + 20; y: -20
            color: "red"
            border { width: 2; color: "white" }
            visible: !dirty
            MouseArea {
                property double strength: 0.25
                anchors.fill: parent; drag.target: parent
                enabled: banner.scale === 0
                onReleased: {
                    whoosh.play()
                    dirty = true
                    ogre.body.linearVelocity = Qt.point(strength*(sight.x-ogre.width/2),
                                                        -1.5*strength*(sight.y-ogre.height/2))
                }
            }
        }
        Shape {
            anchors.fill: parent
            z: -1
            visible: !dirty
            ShapePath {
                startX: ogre.width/2; startY: ogre.height/2
                PathLine { x: sight.x + sight.width/2; y: sight.y + sight.height/2 }
            }
        }
    }
    
    Item {
        id: blocks
        anchors.fill: parent
        Repeater {
            model: level
            Repeater {
                property int outerIndex: index
                model: index
                ImageBody {
                    x: (4-outerIndex)*(width/2)+parent.width-4*width+width*index; y: (parent.height-height)-(4-outerIndex)*height
                    width: ogre.width
                    source: "images/block.png"
                }
            }
        }
    }

    BoxBody { anchors { left: parent.left; right: parent.right; top: parent.bottom; topMargin: -20 } }
    BoxBody { anchors { left: parent.left; right: parent.right; bottom: parent.top } }
    BoxBody { anchors { top: parent.top; bottom: parent.bottom; left: parent.right } }
    BoxBody { anchors { top: parent.top; bottom: parent.bottom; right: parent.left } }
    
    Rectangle {
        width: parent.width/5; height: 35
        anchors { right: parent.right; top: parent.top; rightMargin: 10; topMargin: 10 }
        color: "#472e4a"; radius: 10
        border { width: 2; color: "white" }
        Label { anchors.centerIn: parent; text: "Runs: " + runs; color: "white"; font.bold: true }
    }
    
    Rectangle {
        id: banner
        width: columnLayout.implicitWidth+80; height: columnLayout.implicitHeight+40
        anchors.centerIn: parent
        color: youWon ? "green":"red"; radius: 15
        border { width: 3; color: "white" }
        scale: 1
        visible: scale !== 0
        ColumnLayout {
            id: columnLayout
            anchors.centerIn: parent
            Label {
                text: youWon ? "Congratulations!":"You need to put all blocks on the ground!"; color: "white"
                font { bold: true; pixelSize: refLabel.font.pixelSize*2.5 }
                Layout.alignment: Qt.AlignHCenter 
            }
            Label { id: refLabel; text: youWon ? "Click here to go to the next level":"Click here to try!"; color: "white"; font.bold: true; Layout.alignment: Qt.AlignHCenter }
        }
        Behavior on scale { NumberAnimation { duration: 500; easing.type: Easing.OutBounce } }
        MouseArea {
            anchors.fill: parent
            enabled: parent.scale === 1
            onClicked: {
                banner.scale = 0
                if (youWon) {
                    ogre.x = 20; ogre.y = 411
                    sight.x = ogre.width + 20; sight.y = -20
                    rootItem.source = "images/bg" + Math.trunc(Math.random()*4) + ".png"
                    youWon = false
                    dirty = false
                    level++
                    runs = 0
                }
            }
        }
    }

    DebugDraw { id: debugDraw; opacity: 0.5; z: 1; visible: debugdraw.checked }
}
