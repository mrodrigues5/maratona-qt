import QtQuick 2.15
import Box2D 2.0

Item {
    id: item
    Body {
        target: item
        Box { width: item.width; height: item.height }
    }
}
