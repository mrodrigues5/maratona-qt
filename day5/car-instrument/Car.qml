import QtQuick 2.15
import QtQuick3D 1.15
import QtQuick3D.Materials 1.15

Node {
    id: rootNode
    property alias mainNode: mainNode
    property alias tYRE_MESH_Reduced_001: tYRE_MESH_Reduced_001
    property alias tYRE_MESH_Reduced_002: tYRE_MESH_Reduced_002
    property alias tYRE_MESH_Reduced_003: tYRE_MESH_Reduced_003
    property alias tYRE_MESH_Reduced_004: tYRE_MESH_Reduced_004
    property alias door_Mesh_Reduced_001: door_Mesh_Reduced
    property alias door_Mesh_Reduced_002: door_Mesh_Reduced_002
    property alias door_Mesh_Reduced_003: door_Mesh_Reduced_003
    property alias door_Mesh_Reduced_004: door_Mesh_Reduced_004
    property alias fullsize_doorglass_RL: fullsize_doorglass_RL
    property alias fullsize_doorglass_RR: fullsize_doorglass_RR
    property alias fullsize_doorglass_FL: fullsize_doorglass_FL
    property alias fullsize_doorglass_FR: fullsize_doorglass_FR
    property alias engineHood: engine_Hood_Mesh_Reduced
    property alias tireMaterial: basic_Rubber_Tyre_002_material

    PointLight {
        id: light
        x: 612.796
        y: 184.718
        z: 590.386
        eulerRotation.x: -142.739
        eulerRotation.y: 3.1637
        eulerRotation.z: 106.936
        color: "#ffffffff"
        quadraticFade: 2.22222e-07
    }

    Node {
        id: mainNode

        Node {
            id: sonata_Sport
            scale.x: 100
            scale.y: 100
            scale.z: 100

            Node {
                id: sonata_Sport_001

                Model {
                    id: tYRE_MESH_Reduced_004
                    x: -0.812501
                    y: 1.18289
                    z: 0.384655
                    eulerRotation.x: -90
                    scale.x: -1
                    scale.y: -1
                    scale.z: -1
                    source: "meshes/tYRE_MESH_Reduced_004.mesh"

                    DefaultMaterial {
                        id: basic_Metal__White__002_material
                        diffuseColor: "#ffcccccc"
                    }

                    DefaultMaterial {
                        id: rim__Dark__Metallic__Rough_0_002_material
                        diffuseColor: "#ff373737"
                    }

                    DefaultMaterial {
                        id: half_Chrome_Material_002_material
                        diffuseColor: "#ffcccccc"
                    }

                    DefaultMaterial {
                        id: brake__Metal_0_75__Rough_0_25__002_material
                        diffuseColor: "#ffcc0100"
                    }

                    AluminumMaterial {
                        id: basic_Rubber_Tyre_002_material
                    }
                    materials: [
                        basic_Metal__White__002_material,
                        rim__Dark__Metallic__Rough_0_002_material,
                        half_Chrome_Material_002_material,
                        brake__Metal_0_75__Rough_0_25__002_material,
                        basic_Rubber_Tyre_002_material
                    ]
                }

                Model {
                    id: tYRE_MESH_Reduced_003
                    x: 0.800211
                    y: 1.18289
                    z: 0.384655
                    eulerRotation.x: 90
                    source: "meshes/tYRE_MESH_Reduced_003.mesh"
                    materials: [
                        basic_Metal__White__002_material,
                        rim__Dark__Metallic__Rough_0_002_material,
                        half_Chrome_Material_002_material,
                        brake__Metal_0_75__Rough_0_25__002_material,
                        basic_Rubber_Tyre_002_material
                    ]
                }

                Model {
                    id: tYRE_MESH_Reduced_002
                    x: 0.777436
                    y: -1.75419
                    z: 0.384657
                    eulerRotation.x: 90
                    source: "meshes/tYRE_MESH_Reduced_002.mesh"
                    materials: [
                        basic_Metal__White__002_material,
                        rim__Dark__Metallic__Rough_0_002_material,
                        half_Chrome_Material_002_material,
                        brake__Metal_0_75__Rough_0_25__002_material,
                        basic_Rubber_Tyre_002_material
                    ]
                }

                Model {
                    id: tYRE_MESH_Reduced_001
                    x: -0.789726
                    y: -1.75419
                    z: 0.384657
                    eulerRotation.x: -90
                    scale.x: -1
                    scale.y: -1
                    scale.z: -1
                    source: "meshes/tYRE_MESH_Reduced_001.mesh"
                    materials: [
                        basic_Metal__White__002_material,
                        rim__Dark__Metallic__Rough_0_002_material,
                        half_Chrome_Material_002_material,
                        brake__Metal_0_75__Rough_0_25__002_material,
                        basic_Rubber_Tyre_002_material
                    ]
                }

                Model {
                    id: rear_Bumper_Mesh_Reduced
                    x: -0.0063621
                    y: 2.10965
                    z: 0.548051
                    eulerRotation.x: 90
                    source: "meshes/rear_Bumper_Mesh_Reduced.mesh"

                    DefaultMaterial {
                        id: dark_0_5_Rough_002_material
                        diffuseColor: "#ff000000"
                    }

                    PrincipledMaterial {
                        id: metalic_Car_Paint_material
                        baseColor: "#0055ff"
                        roughness: 0.5
                    }

                    DefaultMaterial {
                        id: red_Reflector_material
                        diffuseColor: "#ff05cc00"
                    }
                    materials: [
                        dark_0_5_Rough_002_material,
                        half_Chrome_Material_002_material,
                        metalic_Car_Paint_material,
                        red_Reflector_material
                    ]
                }

                Model {
                    id: rear_Bonnet_Mesh_Reduced
                    x: -0.00663284
                    y: 2.08167
                    z: 1.1715
                    eulerRotation.x: 90
                    source: "meshes/rear_Bonnet_Mesh_Reduced.mesh"
                    materials: [
                        metalic_Car_Paint_material,
                        half_Chrome_Material_002_material
                    ]
                }

                Model {
                    id: plate
                    x: 2.67541e-05
                    y: 2.2003
                    z: 0.895029
                    eulerRotation.x: 14.5699
                    scale.x: 0.25
                    scale.y: 0.00729642
                    scale.z: 0.0851136
                    source: "meshes/plate.mesh"
                    materials: [
                        dark_0_5_Rough_002_material,
                        half_Chrome_Material_002_material
                    ]
                }

                Model {
                    id: optimize_Mesh_Reduced_001
                    x: 0.941445
                    y: -1.00045
                    z: 1.12933
                    eulerRotation.x: 90
                    source: "meshes/optimize_Mesh_Reduced_001.mesh"

                    DefaultMaterial {
                        id: mirror_004_material
                        diffuseColor: "#ffcccccc"
                    }

                    GlassMaterial {
                        id: noise_Less_Glass_material
                    }
                    materials: [
                        dark_0_5_Rough_002_material,
                        mirror_004_material,
                        noise_Less_Glass_material,
                        red_Reflector_material
                    ]
                }

                Model {
                    id: optimize_Mesh_Reduced
                    x: -0.954667
                    y: -1.00042
                    z: 1.12936
                    eulerRotation.x: 90
                    source: "meshes/optimize_Mesh_Reduced.mesh"
                    materials: [
                        dark_0_5_Rough_002_material,
                        red_Reflector_material,
                        noise_Less_Glass_material,
                        mirror_004_material
                    ]
                }

                Model {
                    id: main_Body_Mesh_Reduced
                    x: -7.45058e-09
                    z: 0.853619
                    eulerRotation.x: 90
                    source: "meshes/main_Body_Mesh_Reduced.mesh"
                    materials: [
                        metalic_Car_Paint_material
                    ]
                }

                Model {
                    id: fullsize_windshield
                    x: -0.00660694
                    y: -1.09137
                    z: 1.28709
                    source: "meshes/fullsize_windshield.mesh"
                    materials: [
                        noise_Less_Glass_material
                    ]
                }

                Model {
                    id: fullsize_doorglass_RR
                    x: -0.751933
                    y: 0.47792
                    z: 1.30559
                    source: "meshes/fullsize_doorglass_RR.mesh"
                    materials: [
                        noise_Less_Glass_material
                    ]
                    Behavior on x { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }

                Model {
                    id: fullsize_doorglass_RL
                    x: 0.738719
                    y: 0.47792
                    z: 1.30559
                    source: "meshes/fullsize_doorglass_RL.mesh"
                    materials: [
                        noise_Less_Glass_material
                    ]
                    Behavior on x { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }

                Model {
                    id: fullsize_doorglass_FR
                    x: -0.777592
                    y: -0.448697
                    z: 1.26511
                    source: "meshes/fullsize_doorglass_FR.mesh"
                    materials: [
                        noise_Less_Glass_material
                    ]
                    Behavior on x { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }

                Model {
                    id: fullsize_doorglass_FL
                    x: 0.764378
                    y: -0.448697
                    z: 1.26511
                    source: "meshes/fullsize_doorglass_FL.mesh"
                    materials: [
                        noise_Less_Glass_material
                    ]
                    Behavior on x { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }

                Model {
                    id: fRONT_BUMPER_MESH_Reduced_001
                    x: -0.0066395
                    y: -2.59786
                    z: 0.568471
                    eulerRotation.x: 90
                    source: "meshes/fRONT_BUMPER_MESH_Reduced_001.mesh"

                    DefaultMaterial {
                        id: metallic_With_0_25_Rough_001_material
                        diffuseColor: "#ff0d0d0d"
                    }

                    DefaultMaterial {
                        id: grill_Metal_Material_material
                        diffuseColor: "#ffcccccc"
                    }
                    materials: [
                        metallic_With_0_25_Rough_001_material,
                        metalic_Car_Paint_material,
                        half_Chrome_Material_002_material,
                        noise_Less_Glass_material,
                        grill_Metal_Material_material,
                        red_Reflector_material
                    ]
                }

                Model {
                    id: engine_Hood_Mesh_Reduced
                    x: -0.00661119
                    y: -2.04826
                    z: 1.02685
                    eulerRotation.x: 90
                    source: "meshes/engine_Hood_Mesh_Reduced.mesh"
                    materials: [
                        metalic_Car_Paint_material
                    ]
                    Behavior on z { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }

                Model {
                    id: door_Mesh_Reduced_004
                    x: -0.871845
                    y: 0.469933
                    z: 0.84615
                    eulerRotation.x: 90
                    source: "meshes/door_Mesh_Reduced_004.mesh"
                    materials: [
                        dark_0_5_Rough_002_material,
                        metalic_Car_Paint_material
                    ]
                    Behavior on x { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }

                Model {
                    id: door_Mesh_Reduced_003
                    x: -0.00614521
                    y: 0.000452727
                    eulerRotation.x: 90
                    source: "meshes/door_Mesh_Reduced_003.mesh"
                    materials: [
                        dark_0_5_Rough_002_material,
                        metalic_Car_Paint_material
                    ]
                    Behavior on x { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }

                Model {
                    id: door_Mesh_Reduced_002
                    x: -0.00614521
                    y: 0.000452727
                    eulerRotation.x: 90
                    source: "meshes/door_Mesh_Reduced_002.mesh"
                    materials: [
                        dark_0_5_Rough_002_material,
                        metalic_Car_Paint_material
                    ]
                    Behavior on x { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }

                Model {
                    id: door_Mesh_Reduced
                    x: 0.858654
                    y: 0.47017
                    z: 0.846445
                    eulerRotation.x: 90
                    source: "meshes/door_Mesh_Reduced.mesh"
                    materials: [
                        dark_0_5_Rough_002_material,
                        metalic_Car_Paint_material
                    ]
                    Behavior on x { NumberAnimation { duration: 1000; easing.type: Easing.OutBounce } }
                }
            }
        }

        Model {
            id: plane
            scale.x: 200
            scale.y: 600
            scale.z: 200
            source: "meshes/plane.mesh"

            DefaultMaterial {
                id: road_3_material
                diffuseMap: Texture {
                    source: "maps/Road002_2K_Color.jpg"
                    tilingModeHorizontal: Texture.Repeat
                    tilingModeVertical: Texture.Repeat
                }
            }
            materials: [
                road_3_material
            ]
        }
    }

    PointLight {
        id: light_001
        x: -528.799
        y: -614.924
        z: 590.386
        eulerRotation.x: -142.739
        eulerRotation.y: 3.1637
        eulerRotation.z: 106.936
        color: "#ffffffff"
        quadraticFade: 2.22222e-07
    }
}
